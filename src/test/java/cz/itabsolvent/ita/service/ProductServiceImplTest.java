package cz.itabsolvent.ita.service;

import cz.itabsolvent.ita.domain.Product;
import cz.itabsolvent.ita.model.ProductDto;
import cz.itabsolvent.ita.repository.ProductRepository;
import cz.itabsolvent.ita.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceImplTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductServiceImpl productService;

    @Test
    public void findAll_ReturnsCollectionOf3ProductDtos() {
        //arrange
        when(productRepository.findAll())
                .thenReturn(List.of(
                        new Product(),
                        new Product()
                ));
        //act
        final List<ProductDto> result = productService.findAll();
        //assert
        assertThat(result.size()).isEqualTo(2);
    }

    @Test
    public void findById_GetProductDtoWithId1_ReturnsProductDtoWithId1() {
        //arrange
        final long id = 1L;
        final Product product = new Product()
                .setId(id);
        when(productRepository.findById(id))
                .thenReturn(Optional.of(product));
        //act
        final ProductDto result = productService.findById(id);
        //assert
        assertThat(result.getId()).isEqualTo(id);
    }
}
