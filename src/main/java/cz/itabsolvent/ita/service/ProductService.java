package cz.itabsolvent.ita.service;

import cz.itabsolvent.ita.model.ProductDto;
import cz.itabsolvent.ita.model.ProductRequestDto;

import java.util.List;

/**
 * ProductService
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2021
 */
public interface ProductService {
    List<ProductDto> findAll();

    ProductDto findById(Long id);

    ProductDto saveProduct(ProductRequestDto request);

    void deleteById(Long id);
}
