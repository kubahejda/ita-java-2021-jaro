package cz.itabsolvent.ita.service.impl;

import cz.itabsolvent.ita.domain.Product;
import cz.itabsolvent.ita.model.ProductDto;
import cz.itabsolvent.ita.model.ProductRequestDto;
import cz.itabsolvent.ita.repository.ProductRepository;
import cz.itabsolvent.ita.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ProductService
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2021
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public List<ProductDto> findAll() {
        return productRepository.findAll().stream()
                .map(this::mapToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(Long id) {
        log.debug("Debug logging");
        log.info("Info logging");
        final Product product = productRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Product " + id + " was not found."));

        return mapToResponse(product);
    }

    @Override
    public ProductDto saveProduct(ProductRequestDto request) {

        final Product product = new Product()
                .setStock(request.getStock())
                .setPrice(request.getPrice())
                .setName(request.getName())
                .setDescription(request.getDescription())
                .setImage(request.getImage());

        final Product savedProduct = productRepository.save(product);

        return mapToResponse(savedProduct);
    }

    @Override
    public void deleteById(Long id) {
        if (!productRepository.existsById(id)) {
            throw new EntityNotFoundException("Product " + id + " was not found.");
        }
        productRepository.deleteById(id);
    }

    private ProductDto mapToResponse(Product savedProduct) {
        return new ProductDto()
                .setStock(savedProduct.getStock())
                .setId(savedProduct.getId())
                .setName(savedProduct.getName())
                .setDescription(savedProduct.getDescription())
                .setImage(savedProduct.getImage())
                .setPrice(savedProduct.getPrice());
    }
}
