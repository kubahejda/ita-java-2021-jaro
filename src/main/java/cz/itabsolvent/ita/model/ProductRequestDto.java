package cz.itabsolvent.ita.model;

import lombok.Data;

import java.math.BigDecimal;

/**
 * ProductRequestDto
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2021
 */
@Data
public class ProductRequestDto {
    private String name;
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;
}
