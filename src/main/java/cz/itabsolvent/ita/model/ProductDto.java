package cz.itabsolvent.ita.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

/**
 * ProductDto
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2021
 */
@Data
@Accessors(chain = true)
public class ProductDto {
    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Long stock;
    private String image;
}
