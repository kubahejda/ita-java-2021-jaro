package cz.itabsolvent.ita.repository;

import cz.itabsolvent.ita.domain.Product;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * ProductRepository
 *
 * @author Jakub Hejda, jakub.hejda@lundegaard.eu, 2021
 */
public interface ProductRepository extends JpaRepository<Product, Long> {
}
